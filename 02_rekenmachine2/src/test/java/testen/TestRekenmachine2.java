package testen;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TestRekenmachine2 {
	private Rekenmachine rekenmachine;

	@BeforeEach
	public void voorElkeTest() {
		rekenmachine = new Rekenmachine();
	}

	@AfterEach
	public void naElkeTest() {
		System.out.println("Weer een test gedaan!");
	}

	@Test
	public void sommeer() {
		int nr1 = 10;
		int nr2 = 50;
		double resultaat = rekenmachine.sommeer(nr1, nr2);
		int expected = 60;
		assertEquals(60, resultaat,
			() -> String.format("De som van %d en %d moet %d zijn", nr1, nr2, expected));
	}

	@Test
	public void vermenigvuldig() {
		double resultaat = rekenmachine.vermenigvuldig(10, 50);
		assertEquals(500, resultaat, "Het product moet <500> zijn");
	}

	@Test
	public void deel() {
		assertThrows(ArithmeticException.class, () -> rekenmachine.deel(10, 0), "Delen door 0 moet een Exception gooien!");
	}


}

