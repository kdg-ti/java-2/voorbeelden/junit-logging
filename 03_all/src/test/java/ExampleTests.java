import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Author: derijkej
 */
public class ExampleTests {

	@Test
	void shouldCheckAllItemsInTheList() {
		List<Integer> numbers = List.of(2, 3, 5, 7);
		Assertions.assertAll(
			// some failing assertions
			//		() -> assertEquals(1, numbers.get(0)),
			//		() -> assertEquals(2, numbers.get(1)),
			() -> assertEquals(2, numbers.get(0)),
			() -> assertEquals(3, numbers.get(1)),
			() -> assertEquals(5, numbers.get(2)),
			() -> assertEquals(7, numbers.get(3)));
	}

}
